package com.fvaldeon.jlistsuprimir;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Created by Profesor on 31/10/2017.
 */
public class Vista {
    private JFrame frame;
    private JPanel panel1;
    private JTextField textField1;
    private JButton almacenarButton;
    private JComboBox comboBox1;
    private JList list1;

    private DefaultComboBoxModel dcbm;
    private DefaultListModel dlm;


    public Vista() {
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        //Creo modelo para el JComboBox
        dcbm = new DefaultComboBoxModel();
        comboBox1.setModel(dcbm);


        //Creo modelo para el JList
        dlm = new DefaultListModel();
        list1.setModel(dlm);


        JMenuBar barraMenus = new JMenuBar();
        JMenu menuArchivo = new JMenu("Archivo");
        JMenuItem itemSalir = new JMenuItem("Salir");
        menuArchivo.add(itemSalir);
        barraMenus.add(menuArchivo);
        frame.setJMenuBar(barraMenus);

        itemSalir.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        almacenarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                dcbm.addElement(textField1.getText());
                dlm.addElement(textField1.getText());
                textField1.setText(null);
            }
        });


        comboBox1.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);

                if(e.getKeyCode() == KeyEvent.VK_DELETE){

                 dcbm.removeElement(comboBox1.getSelectedItem());
                }

            }
        });
        list1.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);

                if(e.getKeyCode() == KeyEvent.VK_DELETE){

                    dlm.removeElement(list1.getSelectedValue());
                }
            }
        });
    }

    public static void main(String[] args) {
       Vista miVista = new Vista();

    }
}
